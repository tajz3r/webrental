namespace WebRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBirthDateToCostumers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Costumers", "BirthDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Costumers", "BirthDate");
        }
    }
}
