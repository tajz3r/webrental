namespace WebRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BirthDateNullableCostumer : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Costumers", "BirthDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Costumers", "BirthDate", c => c.DateTime(nullable: false));
        }
    }
}
