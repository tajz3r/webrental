namespace WebRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GenreAndMoviesTableFix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "ReleasedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Movies", "AddedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Movies", "NumerInStock", c => c.Byte(nullable: false));
            DropColumn("dbo.Genres", "ReleaseDate");
            DropColumn("dbo.Genres", "AddedDate");
            DropColumn("dbo.Genres", "NumberInStock");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Genres", "NumberInStock", c => c.Byte(nullable: false));
            AddColumn("dbo.Genres", "AddedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Genres", "ReleaseDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Movies", "NumerInStock");
            DropColumn("dbo.Movies", "AddedDate");
            DropColumn("dbo.Movies", "ReleasedDate");
        }
    }
}
