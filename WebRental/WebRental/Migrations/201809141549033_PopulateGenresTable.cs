namespace WebRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGenresTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Genres (Id, Name) VALUES (1, 'Comedy')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (2, 'Action')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (3, 'Horror')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (4, 'Thriller')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (5, 'Drama')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (6, 'SienceFiction')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (7, 'Fantasy')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (8, 'Documentary')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (9, 'FairyTail')");
        }
        
        public override void Down()
        {
        }
    }
}
