namespace WebRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CostumerToCustomerNameFixTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Costumers", newName: "Custumers");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Custumers", newName: "Costumers");
        }
    }
}
