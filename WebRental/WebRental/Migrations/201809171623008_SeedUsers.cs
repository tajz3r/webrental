namespace WebRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4cf5c407-1543-4e1f-823c-cfe533c6eec0', N'guest@vidly.com', 0, N'ALss3hoKQhv7EFoe3CzOAexA19/N1AW7Vrmc68njI7LQircy2HHWj/jEB5+lBGrf+g==', N'a81e89fb-27b1-4ae1-83da-21e1fbfce408', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e8db62ae-8073-46b8-af84-1b69df5062b4', N'admin@vidly.com', 0, N'AJg+KpUzqE5snFbr9sng8l92CRCqiorfnlqUdq/RvGLJpEH2hPrQO5QHZZg98z0K1A==', N'2799b5a5-db0c-4f05-8d01-c81719c63aeb', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'cd3a602b-f2d4-4e16-8758-735cbf600cd4', N'CanManageMovies')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e8db62ae-8073-46b8-af84-1b69df5062b4', N'cd3a602b-f2d4-4e16-8758-735cbf600cd4')

");
        }
        
        public override void Down()
        {
        }
    }
}
