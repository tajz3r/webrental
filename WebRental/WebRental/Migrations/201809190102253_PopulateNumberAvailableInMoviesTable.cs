namespace WebRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateNumberAvailableInMoviesTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Movies", "NumerInStock", "NumberInStock");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Movies", "NumberInStock", "NumerInStock");
        }
    }
}
