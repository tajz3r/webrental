﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebRental.Models
{
    public class Min18YearsIfAMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var costumer = (Customer)validationContext.ObjectInstance;
            if (costumer.MembershipTypeId == MembershipType.Unknown || costumer.MembershipTypeId == MembershipType.PayAsYouGo)
                return ValidationResult.Success;

            if (costumer.BirthDate == null)
                return new ValidationResult("Birthdate is required.");

            var age = DateTime.Today.Year - costumer.BirthDate.Value.Year;

            return (age >= 18) 
                ? ValidationResult.Success 
                : new ValidationResult("Costumer should be at least 18 years old to go on a membership.");
        }
    }
}