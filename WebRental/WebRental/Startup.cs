﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebRental.Startup))]
namespace WebRental
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
