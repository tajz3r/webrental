﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebRental.Models;

namespace WebRental.ViewModels
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        public Customer Customer { get; set; }

        public int? Id { get; set; }

        [Required(ErrorMessage = "Please enter costumer's name.")]
        [StringLength(255)]
        public string Name { get; set; }

        [Display(Name = "Date of Birth")]
        [Min18YearsIfAMember]
        public DateTime? BirthDate { get; set; }

        public bool IsSubscribedToNewsletter { get; set; }


        [Required(ErrorMessage = "Please choose membership type.")]
        [Display(Name = "Membership Type")]
        public byte? MembershipTypeId { get; set; }




        public string Title
        {
            get
            {
                return Id != 0 ? "Edit Costumer" : "New Costumer";
            }
        }

        public CustomerFormViewModel()
        {
            Id = 0;
        }

        public CustomerFormViewModel(Customer costumer)
        {
            Id = costumer.Id;
            Name = costumer.Name;
            MembershipTypeId = costumer.MembershipTypeId;
            BirthDate = costumer.BirthDate;
            IsSubscribedToNewsletter = costumer.IsSubscribedToNewsletter;
        }
    }
}